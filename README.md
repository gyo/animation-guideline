# アニメーションガイドライン

## 成果物

アニメーション作成のガイドラインと実装サンプル

- [アニメーションガイドライン](guideline.md)
- [サンプル](http://gyo.gitlab.io/animation-guideline/sample.html)
- [サンプルの補足説明ドキュメント](sample-description.md)

## 開発

```
/
 ├ css/ ...サンプルで使用
 ├ data/ ...サンプル「もっと見る」コンポーネント用jsonデータ
 ├ js/ ...全て結合して script-dist.js として出力。サンプルで使用
 ├ sass/ ...全て結合してcssディレクトリに出力
 ├ guideline.md アニメーションガイドライン
 ├ sample-description.md サンプルの補足説明ドキュメント
 └ sample.html サンプル
```
