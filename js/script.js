//@prepros-prepend foundation/undefined.js
//@prepros-prepend foundation/utility.js
//@prepros-prepend css-variable.js
//@prepros-prepend component/appender.js
//@prepros-prepend component/fade-in.js
//@prepros-prepend component/floating-navigation.js
//@prepros-prepend component/modal.js

const cssVariable = new CssVariable(6, 204);

// accordion
const accordions = q('.accordion');
accordions.forEach(accordion => {
    accordion.classList.add('accordion_is_closed');

    const accordionControls = q('.accordion__control', accordion);
    accordionControls.forEach(accordionControl => {
        accordionControl.addEventListener('click', e => {
            e.preventDefault();
            document.activeElement.blur();
            accordion.classList.toggle('accordion_is_closed');
        });
    });
});

// loader
const forms = q('form');
forms.forEach(form => {
    const loaders = q('.loader', form);
    form.addEventListener('submit', e => {
        e.preventDefault();
        loaders.forEach(loader => {
            loader.classList.remove('loader_is_hidden');
        });
    });
});

// fade in
const fadeInEventHandler = new FadeInEventHandler();
fadeInEventHandler.init();

// float navigation
const floatingNavigationEventHandler = new FloatingNavigationEventHandler();
floatingNavigationEventHandler.init();

// modal
const staticModals = new StaticModals();
staticModals.initModals();

const dynamicModal = new Modal(
    document.querySelector('.js-dynamic-modal'),
    undefined, undefined, undefined, undefined, undefined,
    modal => {
        const modalContent = modal.modalElement.querySelector('.modal__content');
        modalContent.innerHTML = `
            <div class="loader"></div>
        `;
        setTimeout(() => {
            modalContent.innerHTML = `
                <p>${`${Date.now()}`.slice(-5, -3)}</p>

                <button class="filled-button js-dynamic-modal-close">閉じる</button>
            `;
            modal.closeTriggers = Array.from(modal.modalElement.querySelectorAll('.js-dynamic-modal-close'));
            modal.addEventToCloseTriggersClick();
        }, 500);
    },
    undefined, undefined,
    modal => {
        modal.modalElement.querySelector('.modal__content').innerHTML = '';
    }
);
dynamicModal.addEventToBackgroundClick();
dynamicModal.triggers = Array.from(document.querySelectorAll('.js-dynamic-modal-trigger'));
dynamicModal.addEventToTriggersClick();

// appender
const convertFunction = (response, appender) => {
    return response.data.data.map(object => {
        const div = document.createElement('div');
        div.classList.add('stage');
        div.innerText = object.content;
        return div;
    });
};
const afterFunction = appender => {
    fadeInEventHandler.updateFadeInObject();
    fadeInEventHandler.updateFadeInPositionEach();
    fadeInEventHandler.tryFadeOutEach();
}
const appender = new Appender(convertFunction, afterFunction);
appender.addEventToAppenderTrigger();
appender.fetchAndAppendAll();

// slider
var swiper = new Swiper('.swiper-container', {
    pagination: '.swiper-pagination',
    paginationClickable: true,
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    spaceBetween: cssVariable.parseFloat(cssVariable.spaceDouble),
    speed: cssVariable.parseFloat(cssVariable.duration3) * 1000,
    autoplay: cssVariable.parseFloat(cssVariable.duration3) * 1000 * 10,
    loop: true
});