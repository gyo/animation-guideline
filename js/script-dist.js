"use strict";

function _toConsumableArray(e) {
    if (Array.isArray(e)) {
        for (var t = 0, n = Array(e.length); t < e.length; t++) n[t] = e[t];
        return n;
    }
    return Array.from(e);
}

function _classCallCheck(e, t) {
    if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function");
}

var _createClass = function() {
    function e(e, t) {
        for (var n = 0; n < t.length; n++) {
            var a = t[n];
            a.enumerable = a.enumerable || !1, a.configurable = !0, "value" in a && (a.writable = !0), 
            Object.defineProperty(e, a.key, a);
        }
    }
    return function(t, n, a) {
        return n && e(t.prototype, n), a && e(t, a), t;
    };
}();

Number.parseFloat || (Number.parseFloat = parseFloat), Array.from || (Array.from = function() {
    var e = Object.prototype.toString, t = function(t) {
        return "function" == typeof t || "[object Function]" === e.call(t);
    }, n = function(e) {
        var t = Number(e);
        return isNaN(t) ? 0 : 0 !== t && isFinite(t) ? (t > 0 ? 1 : -1) * Math.floor(Math.abs(t)) : t;
    }, a = Math.pow(2, 53) - 1, i = function(e) {
        var t = n(e);
        return Math.min(Math.max(t, 0), a);
    };
    return function(e) {
        var n = this, a = Object(e);
        if (null == e) throw new TypeError("Array.from requires an array-like object - not null or undefined");
        var o, s = arguments.length > 1 ? arguments[1] : void 0;
        if (void 0 !== s) {
            if (!t(s)) throw new TypeError("Array.from: when provided, the second argument must be a function");
            arguments.length > 2 && (o = arguments[2]);
        }
        for (var r, l = i(a.length), d = t(n) ? Object(new n(l)) : new Array(l), c = 0; c < l; ) r = a[c], 
        d[c] = s ? void 0 === o ? s(r, c) : s.call(o, r, c) : r, c += 1;
        return d.length = l, d;
    };
}());

var q = function(e) {
    var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : document;
    return Array.from(t.querySelectorAll(e));
}, CssVariable = function() {
    function e(t, n) {
        _classCallCheck(this, e), this.basePixel = t + "px", this.fontFamily = "Noto Sans Japanese", 
        this.fontSizeSmall = 2 * t + "px", this.fontSize = 3 * t + "px", this.lineHeight = "1.5", 
        this.spaceHalf = 1 * t + "px", this.space = 2 * t + "px", this.spaceDouble = 4 * t + "px", 
        this.round = t / 2 + "px", this.loaderBorderWidth = t / 2 + "px", this.loaderSize = 6 * t + "px", 
        this.fadeInDistance = 4 * t + "px", this.zIndexTooltip = "1", this.zIndexFloatingNavigation = "1", 
        this.zIndexModal = "1", this.huePrimary = "" + n, this.colorPrimary = "hsla(" + n + ", 64%, 48%, 1)", 
        this.colorPrimaryVivid = "hsla(" + n + ", 100%, 48%, 1)", this.colorPrimaryLight = "hsla(" + n + ", 64%, 96%, 1)", 
        this.colorPrimaryTransparent = "hsla(" + n + ", 64%, 48%, 0.5)", this.colorSuccess = "hsla(144, 64%, 48%, 1)", 
        this.colorSuccessVivid = "hsla(144, 100%, 48%, 1)", this.colorSuccessLight = "hsla(144, 64%, 96%, 1)", 
        this.colorSuccessTransparent = "hsla(144, 64%, 48%, 0.5)", this.colorError = "hsla(24, 64%, 48%, 1)", 
        this.colorErrorVivid = "hsla(24, 100%, 48%, 1)", this.colorErrorLight = "hsla(24, 64%, 96%, 1)", 
        this.colorErrorTransparent = "hsla(24, 64%, 48%, 0.5)", this.colorG0 = "hsla(0, 0%, 0%, 1)", 
        this.colorG1 = "hsla(0, 0%, 10%, 1)", this.colorG9 = "hsla(0, 0%, 90%, 1)", this.colorG10 = "hsla(0, 0%, 100%, 1)", 
        this.colorG0Transparent = "hsla(0, 0%, 0%, 0.5)", this.colorG10Transparent = "hsla(0, 0%, 100%, 0.5)", 
        this.shadow0 = "0 0 0 0 rgba(0, 0, 0, 0)", this.shadow1 = "0 " + t / 4 + "px " + t / 4 + "px - 1 0 rgba(0, 0, 0, .3)", 
        this.shadow2 = "0 " + t / 2 + "px " + t / 2 + "px - 1 0 rgba(0, 0, 0, .2)", this.shadow3 = "0 " + t + "px " + t + "px - 1 0 rgba(0, 0, 0, .1)", 
        this.duration1 = "0.125s", this.duration2 = "0.25s", this.duration3 = "0.375s", 
        this.easingAcceleration = "cubic-bezier(0.4, 0.0, 1, 1)", this.easingStandard = "cubic-bezier(0.4, 0.0, 0.2, 1)", 
        this.easingSharp = "cubic-bezier(0.4, 0.0, 0.6, 1)";
    }
    return _createClass(e, [ {
        key: "parseFloat",
        value: function(e) {
            return Number.parseFloat(e.replace(/[a-z]/, ""));
        }
    } ]), e;
}(), Appender = function() {
    function e() {
        var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : function(e, t) {
            return [];
        }, n = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : function(e) {}, a = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : ".appender", i = arguments.length > 3 && void 0 !== arguments[3] ? arguments[3] : ".appender__area", o = arguments.length > 4 && void 0 !== arguments[4] ? arguments[4] : ".appender__trigger", s = arguments.length > 5 && void 0 !== arguments[5] ? arguments[5] : "appender__trigger_is_disappeared", r = arguments.length > 6 && void 0 !== arguments[6] ? arguments[6] : "appender__appended-item", l = arguments.length > 7 && void 0 !== arguments[7] ? arguments[7] : "appender__appended-item_is_disappeared", d = arguments.length > 8 && void 0 !== arguments[8] ? arguments[8] : ".loader", c = arguments.length > 9 && void 0 !== arguments[9] ? arguments[9] : "loader_is_hidden", h = arguments.length > 10 && void 0 !== arguments[10] ? arguments[10] : 0, u = !(arguments.length > 11 && void 0 !== arguments[11]) || arguments[11];
        _classCallCheck(this, e), this.convertResponseToElements = t, this.afterAppendAll = n, 
        this.triggerSelector = a, this.appendAreaSelector = i, this.fetchTriggerSelector = o, 
        this.fetchTriggerDisappearedClass = s, this.appendedItemClass = r, this.appendedItemDisappearedClass = l, 
        this.loaderSelector = d, this.loaderHiddenClass = c, this.root = document.querySelector("" + this.triggerSelector), 
        this.appendArea = this.root.querySelector("" + this.appendAreaSelector), this.fetchTriggers = Array.from(this.root.querySelectorAll("" + this.fetchTriggerSelector)), 
        this.loaders = Array.from(this.root.querySelectorAll("" + this.loaderSelector)), 
        this.page = h, this.next = u, this.fetchedElements = [];
    }
    return _createClass(e, [ {
        key: "fetchAndAppendAll",
        value: function() {
            var e = this;
            document.activeElement.blur(), this.page = this.page + 1, Promise.resolve().then(function() {
                return e.loaders.forEach(function(t) {
                    t.classList.remove(e.loaderHiddenClass);
                }), e.fetch();
            }).then(function(t) {
                return e.next = t.data.next, e.fetchedElements = [].concat(_toConsumableArray(e.fetchedElements), _toConsumableArray(e.convertResponseToElements(t, e))), 
                e.appendAllInOrder();
            }).then(function() {
                e.afterAppendAll(e), !1 === e.next && e.fetchTriggers.forEach(function(t) {
                    t.classList.add("" + e.fetchTriggerDisappearedClass);
                }), e.loaders.forEach(function(t) {
                    t.classList.add(e.loaderHiddenClass);
                });
            });
        }
    }, {
        key: "appendAllInOrder",
        value: function() {
            var e = this;
            return [ function() {
                return new Promise(function(t, n) {
                    e.addDisappearedClassToAllElements(), e.appendAllElements(), t();
                });
            } ].concat(_toConsumableArray(this.fetchedElements.map(function(t) {
                return function() {
                    return new Promise(function(n, a) {
                        setTimeout(function() {
                            t.classList.remove(e.appendedItemDisappearedClass), n();
                        }, 50);
                    });
                };
            })), [ function() {
                e.clearFetchedElements();
            } ]).reduce(function(e, t) {
                return e.then(t);
            }, Promise.resolve());
        }
    }, {
        key: "addDisappearedClassToAllElements",
        value: function() {
            var e = this;
            this.fetchedElements.forEach(function(t) {
                t.classList.add(e.appendedItemClass), t.classList.add(e.appendedItemDisappearedClass);
            });
        }
    }, {
        key: "appendAllElements",
        value: function() {
            var e = document.createDocumentFragment();
            this.fetchedElements.forEach(function(t) {
                e.appendChild(t);
            }), this.appendArea.appendChild(e);
        }
    }, {
        key: "fetch",
        value: function() {
            var e = this;
            return new Promise(function(t, n) {
                axios.get("./data/appender" + e.page + ".json").then(function(e) {
                    setTimeout(function() {
                        t(e);
                    }, 500);
                }).catch(function(e) {
                    n(e);
                });
            });
        }
    }, {
        key: "clearFetchedElements",
        value: function() {
            this.fetchedElements = [];
        }
    }, {
        key: "addEventToAppenderTrigger",
        value: function() {
            var e = this;
            this.fetchTriggers.forEach(function(t) {
                t.addEventListener("click", function(t) {
                    t.preventDefault(), e.fetchAndAppendAll();
                });
            });
        }
    } ]), e;
}(), FadeIn = function() {
    function e(t) {
        var n = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : "fade-in_is_disappeared", a = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : 0, i = arguments.length > 3 && void 0 !== arguments[3] ? arguments[3] : 0, o = arguments.length > 4 && void 0 !== arguments[4] ? arguments[4] : function() {};
        _classCallCheck(this, e), this.element = t, this.disappearedClass = n, this.offset = a, 
        this.delay = i, this.callback = o, this.updatefadeInPosition();
    }
    return _createClass(e, [ {
        key: "updatefadeInPosition",
        value: function() {
            this.fadeInPosition = window.pageYOffset + this.element.getBoundingClientRect().top + this.offset;
        }
    }, {
        key: "tryFadeOut",
        value: function(e) {
            return e + window.innerHeight < this.fadeInPosition && (this.element.classList.add(this.disappearedClass), 
            !0);
        }
    }, {
        key: "tryFadeIn",
        value: function(e) {
            var t = this;
            return !!(this.element.classList.contains(this.disappearedClass) && this.fadeInPosition < e + window.innerHeight) && (setTimeout(function() {
                t.element.classList.remove(t.disappearedClass), t.callback();
            }, this.delay), !0);
        }
    } ]), e;
}(), FadeInEventHandler = function() {
    function e() {
        var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "fade-in";
        _classCallCheck(this, e), this.triggerClass = t, this.FadeInObjects = [];
    }
    return _createClass(e, [ {
        key: "init",
        value: function() {
            var e = !(arguments.length > 0 && void 0 !== arguments[0]) || arguments[0], t = !(arguments.length > 1 && void 0 !== arguments[1]) || arguments[1];
            this.updateFadeInObject(), this.tryFadeOutEach(), e && this.addEventToWindowScroll(), 
            t && this.addEventToWindowResize();
        }
    }, {
        key: "updateFadeInObject",
        value: function() {
            this.FadeInObjects = Array.from(document.querySelectorAll("." + this.triggerClass)).map(function(e) {
                return new FadeIn(e);
            });
        }
    }, {
        key: "addEventToWindowScroll",
        value: function() {
            var e = this;
            window.addEventListener("scroll", function() {
                e.tryFadeInEach();
            });
        }
    }, {
        key: "addEventToWindowResize",
        value: function() {
            var e = this;
            window.addEventListener("resize", function() {
                e.updateFadeInPositionEach(), e.tryFadeInEach();
            });
        }
    }, {
        key: "tryFadeOutEach",
        value: function() {
            var e = window.pageYOffset;
            this.FadeInObjects.forEach(function(t, n) {
                t.tryFadeOut(e);
            });
        }
    }, {
        key: "tryFadeInEach",
        value: function() {
            var e = window.pageYOffset;
            this.FadeInObjects.forEach(function(t, n) {
                t.tryFadeIn(e);
            });
        }
    }, {
        key: "updateFadeInPositionEach",
        value: function() {
            this.FadeInObjects.forEach(function(e, t) {
                e.updatefadeInPosition();
            });
        }
    } ]), e;
}(), FloatingNavigation = function() {
    function e(t) {
        var n = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 300;
        _classCallCheck(this, e), this.element = t, this.floatingPosition = n, this.disappearedClass = "floating-navigation_is_disappeared";
    }
    return _createClass(e, [ {
        key: "trySlideToggle",
        value: function(e) {
            e < this.floatingPosition ? this.element.classList.add(this.disappearedClass) : this.floatingPosition < e && this.element.classList.remove(this.disappearedClass);
        }
    } ]), e;
}(), FloatingNavigationEventHandler = function() {
    function e() {
        _classCallCheck(this, e), this.triggerClass = "floating-navigation", this.floatingNavigationObjects = Array.from(document.querySelectorAll("." + this.triggerClass)).map(function(e) {
            return new FloatingNavigation(e);
        });
    }
    return _createClass(e, [ {
        key: "init",
        value: function() {
            var e = !(arguments.length > 0 && void 0 !== arguments[0]) || arguments[0];
            this.trySlideToggle(), e && this.addEventToWindowScroll();
        }
    }, {
        key: "addEventToWindowScroll",
        value: function() {
            var e = this;
            window.addEventListener("scroll", function() {
                e.trySlideToggle();
            });
        }
    }, {
        key: "trySlideToggle",
        value: function() {
            var e = window.pageYOffset;
            this.floatingNavigationObjects.forEach(function(t) {
                t.trySlideToggle(e);
            });
        }
    } ]), e;
}(), Modal = function() {
    function e(t) {
        var n = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : [], a = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : [], i = arguments.length > 3 && void 0 !== arguments[3] ? arguments[3] : "modal_is_hidden", o = arguments.length > 4 && void 0 !== arguments[4] ? arguments[4] : "modal_with_open-transition", s = arguments.length > 5 && void 0 !== arguments[5] ? arguments[5] : "modal_with_close-transition", r = arguments.length > 6 && void 0 !== arguments[6] ? arguments[6] : function(e) {}, l = arguments.length > 7 && void 0 !== arguments[7] ? arguments[7] : function(e) {}, d = arguments.length > 8 && void 0 !== arguments[8] ? arguments[8] : function(e) {}, c = arguments.length > 9 && void 0 !== arguments[9] ? arguments[9] : function(e) {};
        _classCallCheck(this, e), this._modalElement = t, this._triggers = n, this._closeTriggers = a, 
        this._hiddenClass = i, this._openTransitionClass = o, this._closeTransitionClass = s, 
        this._beforeOpenFunction = r, this._afterOpenFunction = l, this._beforeCloseFunction = d, 
        this._afterCloseFunction = c;
    }
    return _createClass(e, [ {
        key: "init",
        value: function() {
            var e = !(arguments.length > 0 && void 0 !== arguments[0]) || arguments[0], t = !(arguments.length > 1 && void 0 !== arguments[1]) || arguments[1], n = !(arguments.length > 2 && void 0 !== arguments[2]) || arguments[2];
            e && this.addEventToTriggersClick(), t && this.addEventToCloseTriggersClick(), n && this.addEventToBackgroundClick();
        }
    }, {
        key: "addEventToTriggersClick",
        value: function() {
            var e = this;
            this._triggers.forEach(function(t) {
                t.addEventListener("click", function(t) {
                    t.preventDefault(), e.open();
                });
            });
        }
    }, {
        key: "addEventToCloseTriggersClick",
        value: function() {
            var e = this;
            this._closeTriggers.forEach(function(t) {
                t.addEventListener("click", function(t) {
                    t.preventDefault(), e.close();
                });
            });
        }
    }, {
        key: "addEventToBackgroundClick",
        value: function() {
            var e = this;
            this._modalElement.addEventListener("click", function(t) {
                t.preventDefault(), t.target === e._modalElement && e.close();
            });
        }
    }, {
        key: "open",
        value: function() {
            document.activeElement.blur(), this._beforeOpenFunction(this), this._modalElement.classList.add(this._openTransitionClass), 
            this._modalElement.classList.remove(this._closeTransitionClass), this._modalElement.classList.remove(this._hiddenClass), 
            this._afterOpenFunction(this);
        }
    }, {
        key: "close",
        value: function() {
            document.activeElement.blur(), this._beforeCloseFunction(this), this._modalElement.classList.remove(this._openTransitionClass), 
            this._modalElement.classList.add(this._closeTransitionClass), this._modalElement.classList.add(this._hiddenClass), 
            this._afterCloseFunction(this);
        }
    }, {
        key: "modalElement",
        get: function() {
            return this._modalElement;
        }
    }, {
        key: "triggers",
        set: function(e) {
            this._triggers = e;
        }
    }, {
        key: "closeTriggers",
        set: function(e) {
            this._closeTriggers = e;
        }
    } ]), e;
}(), StaticModals = function() {
    function e() {
        var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : ".js-modal";
        _classCallCheck(this, e), this._modalElementSelector = t, this._modals = [];
    }
    return _createClass(e, [ {
        key: "initModals",
        value: function() {
            this._modals = Array.from(document.querySelectorAll("" + this._modalElementSelector)).map(function(e) {
                return new Modal(e, Array.from(document.querySelectorAll("" + e.dataset.modalTrigger)), Array.from(document.querySelectorAll("" + e.dataset.modalCloseTrigger)), e.dataset.modalHiddenClass, e.dataset.modalOpenTransitionClass, e.dataset.modalCloseTransitionClass);
            }), this._modals.forEach(function(e) {
                e.init("false" !== e.modalElement.dataset.modalAddEventToTriggerClick, "false" !== e.modalElement.dataset.modalAddEventToCloseTriggerClick, "false" !== e.modalElement.dataset.modalAddEventToBackgroundClick);
            });
        }
    } ]), e;
}(), cssVariable = new CssVariable(6, 204), accordions = q(".accordion");

accordions.forEach(function(e) {
    e.classList.add("accordion_is_closed"), q(".accordion__control", e).forEach(function(t) {
        t.addEventListener("click", function(t) {
            t.preventDefault(), document.activeElement.blur(), e.classList.toggle("accordion_is_closed");
        });
    });
});

var forms = q("form");

forms.forEach(function(e) {
    var t = q(".loader", e);
    e.addEventListener("submit", function(e) {
        e.preventDefault(), t.forEach(function(e) {
            e.classList.remove("loader_is_hidden");
        });
    });
});

var fadeInEventHandler = new FadeInEventHandler();

fadeInEventHandler.init();

var floatingNavigationEventHandler = new FloatingNavigationEventHandler();

floatingNavigationEventHandler.init();

var staticModals = new StaticModals();

staticModals.initModals();

var dynamicModal = new Modal(document.querySelector(".js-dynamic-modal"), void 0, void 0, void 0, void 0, void 0, function(e) {
    var t = e.modalElement.querySelector(".modal__content");
    t.innerHTML = '\n            <div class="loader"></div>\n        ', setTimeout(function() {
        t.innerHTML = "\n                <p>" + ("" + Date.now()).slice(-5, -3) + '</p>\n\n                <button class="filled-button js-dynamic-modal-close">閉じる</button>\n            ', 
        e.closeTriggers = Array.from(e.modalElement.querySelectorAll(".js-dynamic-modal-close")), 
        e.addEventToCloseTriggersClick();
    }, 500);
}, void 0, void 0, function(e) {
    e.modalElement.querySelector(".modal__content").innerHTML = "";
});

dynamicModal.addEventToBackgroundClick(), dynamicModal.triggers = Array.from(document.querySelectorAll(".js-dynamic-modal-trigger")), 
dynamicModal.addEventToTriggersClick();

var convertFunction = function(e, t) {
    return e.data.data.map(function(e) {
        var t = document.createElement("div");
        return t.classList.add("stage"), t.innerText = e.content, t;
    });
}, afterFunction = function(e) {
    fadeInEventHandler.updateFadeInObject(), fadeInEventHandler.updateFadeInPositionEach(), 
    fadeInEventHandler.tryFadeOutEach();
}, appender = new Appender(convertFunction, afterFunction);

appender.addEventToAppenderTrigger(), appender.fetchAndAppendAll();

var swiper = new Swiper(".swiper-container", {
    pagination: ".swiper-pagination",
    paginationClickable: !0,
    nextButton: ".swiper-button-next",
    prevButton: ".swiper-button-prev",
    spaceBetween: cssVariable.parseFloat(cssVariable.spaceDouble),
    speed: 1e3 * cssVariable.parseFloat(cssVariable.duration3),
    autoplay: 1e3 * cssVariable.parseFloat(cssVariable.duration3) * 10,
    loop: !0
});
//# sourceMappingURL=script-dist.js.map