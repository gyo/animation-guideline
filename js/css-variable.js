class CssVariable {

    constructor(basePixel, huePrimary) {

        this.basePixel = `${basePixel}px`;

        this.fontFamily = `Noto Sans Japanese`;

        // structure

        this.fontSizeSmall = `${basePixel * 2}px`;
        this.fontSize = `${basePixel * 3}px`;

        this.lineHeight = `1.5`;

        this.spaceHalf = `${basePixel * 1}px`;
        this.space = `${basePixel * 2}px`;
        this.spaceDouble = `${basePixel * 4}px`;

        this.round = `${basePixel / 2}px`;

        this.loaderBorderWidth = `${basePixel / 2}px`;
        this.loaderSize = `${basePixel * 6}px`;

        this.fadeInDistance = `${basePixel * 4}px`;

        // z-index
        // swiper.cssでz-indexの指定あり。.swiper-container { z-index:1 };
        this.zIndexTooltip = `1`;
        this.zIndexFloatingNavigation = `1`;
        this.zIndexModal = `1`;

        // skin
        // this.huePrimary = `84`;
        // this.huePrimary = `144`;
        this.huePrimary = `${huePrimary}`;
        // this.huePrimary = `264`;
        // this.huePrimary = `324`;

        this.colorPrimary = `hsla(${huePrimary}, 64%, 48%, 1)`;
        this.colorPrimaryVivid = `hsla(${huePrimary}, 100%, 48%, 1)`;
        this.colorPrimaryLight = `hsla(${huePrimary}, 64%, 96%, 1)`;
        this.colorPrimaryTransparent = `hsla(${huePrimary}, 64%, 48%, 0.5)`;
        this.colorSuccess = `hsla(144, 64%, 48%, 1)`;
        this.colorSuccessVivid = `hsla(144, 100%, 48%, 1)`;
        this.colorSuccessLight = `hsla(144, 64%, 96%, 1)`;
        this.colorSuccessTransparent = `hsla(144, 64%, 48%, 0.5)`;
        this.colorError = `hsla(24, 64%, 48%, 1)`;
        this.colorErrorVivid = `hsla(24, 100%, 48%, 1)`;
        this.colorErrorLight = `hsla(24, 64%, 96%, 1)`;
        this.colorErrorTransparent = `hsla(24, 64%, 48%, 0.5)`;
        this.colorG0 = `hsla(0, 0%, 0%, 1)`;
        this.colorG1 = `hsla(0, 0%, 10%, 1)`;
        this.colorG9 = `hsla(0, 0%, 90%, 1)`;
        this.colorG10 = `hsla(0, 0%, 100%, 1)`;
        this.colorG0Transparent = `hsla(0, 0%, 0%, 0.5)`;
        this.colorG10Transparent = `hsla(0, 0%, 100%, 0.5)`;

        this.shadow0 = `0 0 0 0 rgba(0, 0, 0, 0)`;
        this.shadow1 = `0 ${basePixel / 4}px ${basePixel / 4}px - 1 0 rgba(0, 0, 0, .3)`;
        this.shadow2 = `0 ${basePixel / 2}px ${basePixel / 2}px - 1 0 rgba(0, 0, 0, .2)`;
        this.shadow3 = `0 ${basePixel}px ${basePixel}px - 1 0 rgba(0, 0, 0, .1)`;

        // animation

        this.duration1 = `0.125s`;
        this.duration2 = `0.25s`;
        this.duration3 = `0.375s`;

        this.easingAcceleration = `cubic-bezier(0.4, 0.0, 1, 1)`;
        this.easingStandard = `cubic-bezier(0.4, 0.0, 0.2, 1)`;
        this.easingSharp = `cubic-bezier(0.4, 0.0, 0.6, 1)`;
    }

    parseFloat(string) {
        return Number.parseFloat(string.replace(/[a-z]/, ''));
    }
}