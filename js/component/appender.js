class Appender {

    constructor(
        convertResponseToElements = (response, appender) => {
            return [];
        },
        afterAppendAll = (appender) => {},
        triggerSelector = '.appender',
        appendAreaSelector = '.appender__area',
        fetchTriggerSelector = '.appender__trigger',
        fetchTriggerDisappearedClass = 'appender__trigger_is_disappeared',
        appendedItemClass = 'appender__appended-item',
        appendedItemDisappearedClass = 'appender__appended-item_is_disappeared',
        loaderSelector = '.loader',
        loaderHiddenClass = 'loader_is_hidden',
        page = 0,
        next = true
    ) {
        this.convertResponseToElements = convertResponseToElements;
        this.afterAppendAll = afterAppendAll;
        this.triggerSelector = triggerSelector;
        this.appendAreaSelector = appendAreaSelector;
        this.fetchTriggerSelector = fetchTriggerSelector;
        this.fetchTriggerDisappearedClass = fetchTriggerDisappearedClass;
        this.appendedItemClass = appendedItemClass;
        this.appendedItemDisappearedClass = appendedItemDisappearedClass;
        this.loaderSelector = loaderSelector;
        this.loaderHiddenClass = loaderHiddenClass;

        this.root = document.querySelector(`${this.triggerSelector}`);
        this.appendArea = this.root.querySelector(`${this.appendAreaSelector}`);
        this.fetchTriggers = Array.from(this.root.querySelectorAll(`${this.fetchTriggerSelector}`));
        this.loaders = Array.from(this.root.querySelectorAll(`${this.loaderSelector}`));

        this.page = page;
        this.next = next;
        this.fetchedElements = [];
    };

    fetchAndAppendAll() {
        document.activeElement.blur();
        this.page = this.page + 1;
        Promise.resolve()
            .then(() => {
                this.loaders.forEach(loader => {
                    loader.classList.remove(this.loaderHiddenClass);
                });
                return this.fetch();
            })
            .then(response => {
                this.next = response.data.next;
                this.fetchedElements = [
                    ...this.fetchedElements,
                    ...this.convertResponseToElements(response, this)
                ];
                return this.appendAllInOrder();
            })
            .then(() => {
                this.afterAppendAll(this);
                if (this.next === false) {
                    this.fetchTriggers.forEach(fetchTrigger => {
                        fetchTrigger.classList.add(`${this.fetchTriggerDisappearedClass}`);
                    });
                }
                this.loaders.forEach(loader => {
                    loader.classList.add(this.loaderHiddenClass);
                });
            });
    }

    appendAllInOrder() {
        return [
            () => {
                return new Promise((resolve, reject) => {
                    this.addDisappearedClassToAllElements();
                    this.appendAllElements();
                    resolve();
                });
            },
            ...this.fetchedElements.map(fetchedElement => {
                return () => {
                    return new Promise((resolve, reject) => {
                        setTimeout(() => {
                            fetchedElement.classList.remove(this.appendedItemDisappearedClass);
                            resolve();
                        }, 50);
                    });
                };
            }),
            () => {
                this.clearFetchedElements();
            }
        ].reduce((result, current) => {
            return result.then(current);
        }, Promise.resolve());
    }

    addDisappearedClassToAllElements() {
        this.fetchedElements.forEach(fetchedElement => {
            fetchedElement.classList.add(this.appendedItemClass);
            fetchedElement.classList.add(this.appendedItemDisappearedClass);
        });
    }

    appendAllElements() {
        const fragment = document.createDocumentFragment();
        this.fetchedElements.forEach(fetchedElement => {
            fragment.appendChild(fetchedElement);
        });
        this.appendArea.appendChild(fragment);
    }

    fetch() {
        return new Promise((resolve, reject) => {
            axios.get(`./data/appender${this.page}.json`)
                .then(response => {
                    setTimeout(() => {
                        resolve(response);
                    }, 500);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    clearFetchedElements() {
        this.fetchedElements = [];
    }

    addEventToAppenderTrigger() {
        this.fetchTriggers.forEach(fetchTrigger => {
            fetchTrigger.addEventListener('click', e => {
                e.preventDefault();
                this.fetchAndAppendAll();
            });
        });
    }
}
