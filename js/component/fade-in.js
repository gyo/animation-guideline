class FadeIn {

    constructor(
        element,
        disappearedClass = 'fade-in_is_disappeared',
        offset = 0,
        delay = 0,
        callback = () => { }
    ) {
        this.element = element;
        this.disappearedClass = disappearedClass;
        this.offset = offset;
        this.delay = delay;
        this.callback = callback;

        this.updatefadeInPosition();
    };

    updatefadeInPosition() {
        this.fadeInPosition = window.pageYOffset + this.element.getBoundingClientRect().top + this.offset;
    };

    tryFadeOut(scrollTop) {
        if (scrollTop + window.innerHeight < this.fadeInPosition) {
            this.element.classList.add(this.disappearedClass);
            return true;
        } else {
            return false;
        }
    };

    tryFadeIn(scrollTop) {
        if (this.element.classList.contains(this.disappearedClass) && this.fadeInPosition < scrollTop + window.innerHeight) {
            setTimeout(() => {
                this.element.classList.remove(this.disappearedClass);
                this.callback();
            }, this.delay);
            return true;
        } else {
            return false;
        }
    };
}

class FadeInEventHandler {

    constructor(
        triggerClass = 'fade-in'
    ) {
        this.triggerClass = triggerClass;
        this.FadeInObjects = [];
    };

    init(addEventToWindowScroll = true, addEventToWindowResize = true) {
        this.updateFadeInObject();

        this.tryFadeOutEach();

        if (addEventToWindowScroll) {
            this.addEventToWindowScroll();
        }

        if (addEventToWindowResize) {
            this.addEventToWindowResize();
        }
    }

    updateFadeInObject() {
        this.FadeInObjects = Array.from(document.querySelectorAll(`.${this.triggerClass}`)).map(element => {
            return new FadeIn(element);
        });
    }

    addEventToWindowScroll() {
        window.addEventListener('scroll', () => {
            this.tryFadeInEach();
        });
    };

    addEventToWindowResize() {
        window.addEventListener('resize', () => {
            this.updateFadeInPositionEach();
            this.tryFadeInEach();
        });
    };

    tryFadeOutEach() {
        const scrollTop = window.pageYOffset;
        this.FadeInObjects.forEach((FadeIn, index) => {
            FadeIn.tryFadeOut(scrollTop);
        });
    }

    tryFadeInEach() {
        const scrollTop = window.pageYOffset;
        this.FadeInObjects.forEach((FadeIn, index) => {
            FadeIn.tryFadeIn(scrollTop);
        });
    }

    updateFadeInPositionEach() {
        this.FadeInObjects.forEach((FadeIn, index) => {
            FadeIn.updatefadeInPosition();
        });
    }
}
