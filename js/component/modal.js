/**
 * モーダルの挙動
 */
class Modal {

    /**
     *
     * @param {Node} modalElement
     * @param {Array<Node>} triggers
     * @param {Array<Node>} closeTriggers
     * @param {String} hiddenClass
     * @param {String} openTransitionClass
     * @param {String} closeTransitionClass
     * @param {Function} beforeOpenFunction
     * @param {Function} afterOpenFunction
     * @param {Function} beforeCloseFunction
     * @param {Function} afterCloseFunction
     */
    constructor(
        modalElement,
        triggers = [],
        closeTriggers = [],
        hiddenClass = 'modal_is_hidden',
        openTransitionClass = 'modal_with_open-transition',
        closeTransitionClass = 'modal_with_close-transition',
        beforeOpenFunction = modal => { },
        afterOpenFunction = modal => { },
        beforeCloseFunction = modal => { },
        afterCloseFunction = modal => { }
    ) {
        this._modalElement = modalElement;
        this._triggers = triggers;
        this._closeTriggers = closeTriggers;
        this._hiddenClass = hiddenClass;
        this._openTransitionClass = openTransitionClass;
        this._closeTransitionClass = closeTransitionClass;
        this._beforeOpenFunction = beforeOpenFunction;
        this._afterOpenFunction = afterOpenFunction;
        this._beforeCloseFunction = beforeCloseFunction;
        this._afterCloseFunction = afterCloseFunction;
    };

    get modalElement() {
        return this._modalElement;
    }

    set triggers(triggers) {
        this._triggers = triggers;
    }

    set closeTriggers(closeTriggers) {
        this._closeTriggers = closeTriggers;
    }

    /**
     *
     * @param {Boolean} addEventToTriggersClick
     * @param {Boolean} addEventToCloseTriggersClick
     * @param {Boolean} addEventToBackgroundClick
     */
    init(
        addEventToTriggersClick = true,
        addEventToCloseTriggersClick = true,
        addEventToBackgroundClick = true
    ) {
        if (addEventToTriggersClick) {
            this.addEventToTriggersClick();
        }

        if (addEventToCloseTriggersClick) {
            this.addEventToCloseTriggersClick();
        }

        if (addEventToBackgroundClick) {
            this.addEventToBackgroundClick();
        }
    };

    addEventToTriggersClick() {
        this._triggers.forEach(trigger => {
            trigger.addEventListener('click', e => {
                e.preventDefault();

                this.open();
            });
        });
    }

    addEventToCloseTriggersClick() {
        this._closeTriggers.forEach(closeTrigger => {
            closeTrigger.addEventListener('click', e => {
                e.preventDefault();

                this.close();
            });
        });
    }

    addEventToBackgroundClick() {
        this._modalElement.addEventListener('click', e => {
            e.preventDefault();

            if (e.target === this._modalElement) {
                this.close();
            }
        });
    }

    open() {
        document.activeElement.blur();
        this._beforeOpenFunction(this);
        this._modalElement.classList.add(this._openTransitionClass);
        this._modalElement.classList.remove(this._closeTransitionClass);
        this._modalElement.classList.remove(this._hiddenClass);
        this._afterOpenFunction(this);
    }

    close() {
        document.activeElement.blur();
        this._beforeCloseFunction(this);
        this._modalElement.classList.remove(this._openTransitionClass);
        this._modalElement.classList.add(this._closeTransitionClass);
        this._modalElement.classList.add(this._hiddenClass);
        this._afterCloseFunction(this);
    }
}

/**
 * data属性をもとにModalのoptionを指定する。beforeOpenFunctionなどを必要としないもののみ
 * 利用するdata属性は以下の通り
 * modalTrigger：セレクタを指定
 * modalCloseTrigger：セレクタを指定
 * modalHiddenClass
 * modalOpenTransitionClass
 * modalCloseTransitionClass
 * modalAddEventToTriggerClick
 * modalAddEventToCloseTriggerClick
 * modalAddEventToBackgroundClick
 */
class StaticModals {

    /**
     *
     * @param {String} modalElementSelector
     */
    constructor(
        modalElementSelector = '.js-modal'
    ) {
        this._modalElementSelector = modalElementSelector;
        this._modals = [];
    }

    initModals() {
        this._modals = Array.from(document.querySelectorAll(`${this._modalElementSelector}`)).map(modalElement => {
            return new Modal(
                modalElement,
                Array.from(document.querySelectorAll(`${modalElement.dataset.modalTrigger}`)),
                Array.from(document.querySelectorAll(`${modalElement.dataset.modalCloseTrigger}`)),
                modalElement.dataset.modalHiddenClass,
                modalElement.dataset.modalOpenTransitionClass,
                modalElement.dataset.modalCloseTransitionClass
            );
        });

        this._modals.forEach(modal => {
            modal.init(
                modal.modalElement.dataset.modalAddEventToTriggerClick !== 'false',
                modal.modalElement.dataset.modalAddEventToCloseTriggerClick !== 'false',
                modal.modalElement.dataset.modalAddEventToBackgroundClick !== 'false'
            );
        });
    }
}