class FloatingNavigation {

    constructor(element, floatingPosition = 300) {
        this.element = element;
        this.floatingPosition = floatingPosition;

        this.disappearedClass = 'floating-navigation_is_disappeared';
    };

    trySlideToggle(scrollTop) {
        if (scrollTop < this.floatingPosition) {
            this.element.classList.add(this.disappearedClass);
        } else if (this.floatingPosition < scrollTop) {
            this.element.classList.remove(this.disappearedClass);
        }
    };
}

class FloatingNavigationEventHandler {

    constructor() {
        this.triggerClass = 'floating-navigation';

        this.floatingNavigationObjects = Array.from(document.querySelectorAll(`.${this.triggerClass}`)).map(element => {
            return new FloatingNavigation(element);
        });
    };

    init(addEventToWindowScroll = true) {
        this.trySlideToggle();

        if (addEventToWindowScroll) {
            this.addEventToWindowScroll();
        }
    }

    addEventToWindowScroll() {
        window.addEventListener('scroll', () => {
            this.trySlideToggle();
        });
    };

    trySlideToggle() {
        const scrollTop = window.pageYOffset;
        this.floatingNavigationObjects.forEach(floatingNavigation => {
            floatingNavigation.trySlideToggle(scrollTop);
        });
    }
}
